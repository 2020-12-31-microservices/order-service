package com.micro.order.app.controller;

import com.micro.order.app.common.Payment;
import com.micro.order.app.common.PaymentOrder;
import com.micro.order.app.common.PaymentRequest;
import com.micro.order.app.common.PaymentServiceProxy;
import com.micro.order.app.entity.Order;
import com.micro.order.app.request.CreateOrderRequest;
import com.micro.order.app.response.ApiResponse;
import com.micro.order.app.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api")
public class OrderController {

    @Autowired
    private OrderService orderService;
    @Autowired private PaymentServiceProxy paymentServiceProxy;

    Logger logger = LoggerFactory.getLogger(OrderController.class);

    @PostMapping(
            value = {"/order"},
            produces = {"application/json"},
            consumes = {"application/json"}
    )
    public ResponseEntity<ApiResponse> createOrder(@Valid @RequestBody CreateOrderRequest request) {
        Order order = orderService.createOrder(request);
        PaymentRequest paymentRequest = new PaymentRequest(UUID.randomUUID().toString(), order.getId(), order.getPrice() * order.getQty());
        ApiResponse<Order> apiResponse = new ApiResponse<Order>(201, true, order);
        paymentServiceProxy.createOrder(paymentRequest);
        return new ResponseEntity<ApiResponse>(apiResponse, HttpStatus.CREATED);
    }

    @GetMapping(
            value = {"/order/{id}"},
            produces = {"application/json"}
    )
    public ResponseEntity<ApiResponse> getOrderById(@PathVariable("id") String id) {
        Order order = orderService.getOrderById(id);
        Payment payment = paymentServiceProxy.getPaymentByOrderId(id);
        logger.info("payment : {}", payment);

        PaymentOrder response = new PaymentOrder(order, payment);
        ApiResponse<PaymentOrder> apiResponse = new ApiResponse<PaymentOrder>(200, true, response);

        return new ResponseEntity<ApiResponse>(apiResponse, HttpStatus.OK);
    }

    @GetMapping(
            value = {"/order"},
            produces = {"application/json"}
    )
    public ResponseEntity<ApiResponse> getAllOrders() {
        List<Order> orders = orderService.getAllOrders();
        ApiResponse<Object> apiResponse = new ApiResponse<Object>(200, true, orders);

        return new ResponseEntity<ApiResponse>(apiResponse, HttpStatus.OK);
    }

    @GetMapping(
            value = {"/order/delete"},
            produces = {"application/json"}
    )
    public ResponseEntity<ApiResponse> deleteAllOrder() {
        orderService.deleteAllOrders();
        ApiResponse<Object> apiResponse = new ApiResponse<Object>(200, true, "Successfully delete orders");

        return new ResponseEntity<ApiResponse>(apiResponse, HttpStatus.OK);
    }


}