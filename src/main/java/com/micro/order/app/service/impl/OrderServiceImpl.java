package com.micro.order.app.service.impl;

import com.micro.order.app.entity.Order;
import com.micro.order.app.exception.NotFoundException;
import com.micro.order.app.repository.OrderRepository;
import com.micro.order.app.request.CreateOrderRequest;
import com.micro.order.app.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderRepository orderRepository;

    @Override
    public Order createOrder(CreateOrderRequest createOrderRequest) {
        Order order = new Order();
        order.setName(createOrderRequest.getName());
        order.setQty(createOrderRequest.getQty());
        order.setPrice(createOrderRequest.getPrice());
        order.setCreatedAt(new Date());
        order.setUpdatedAt(new Date());

        orderRepository.save(order);

        return order;
    }

    @Override
    public Order getOrderById(String id) {
        Order order =findOrderByIdOrThrowNotFound(id);

        return order;
    }

    @Override
    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    private Order findOrderByIdOrThrowNotFound(String id) {
        Order order = orderRepository.findById(id).orElse(null);

        if (order == null) {
            throw new NotFoundException();
        } else {
            return order;
        }
    }

    @Override
    public boolean deleteAllOrders() {
        orderRepository.deleteAll();

        return true;
    }
}
