package com.micro.order.app.service;

import com.micro.order.app.entity.Order;
import com.micro.order.app.request.CreateOrderRequest;

import java.util.List;

public interface OrderService {

    public Order createOrder(CreateOrderRequest createOrderRequest);

    public Order getOrderById(String id);

    public List<Order> getAllOrders();

    public boolean deleteAllOrders();
}
