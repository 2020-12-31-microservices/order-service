package com.micro.order.app.common;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@FeignClient(name = "cloud-gateway")
public interface PaymentServiceProxy {

    @PostMapping("/payment-service/api/payment")
    public void createOrder(@Valid @RequestBody PaymentRequest paymentRequest);

    @GetMapping("/payment-service/api/payment/order/{orderId}")
    public Payment getPaymentByOrderId(@PathVariable("orderId") String orderId);
}
