package com.micro.order.app.common;

import com.micro.order.app.entity.Order;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentOrder {

    private Order order;

    private Payment payment;
}
