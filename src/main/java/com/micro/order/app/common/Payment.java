package com.micro.order.app.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Payment {

    private String id;

    private String invoice;

    private String orderId;

    private double amount;

    private Date createdAt;

    private Date updatedAt;

}
